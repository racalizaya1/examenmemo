<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicationTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunication_text', function (Blueprint $table) {
            $table->bigIncrements('id_comunication_text');

            $table->string('body_text',200)->nullable(true);
            $table->string('phone_client',200)->nullable(true);
            $table->date('date_text')->nullable(true);
            $table->boolean('status_text')->nullable(true);
            $table->string('type_direction',100)->nullable(true);
            $table->string('id_user',100)->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunication_text');
    }
}
