<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_email', function (Blueprint $table) {
            $table->bigIncrements('id_communication_email');

            $table->text('body_email')->nullable(true);
            $table->date('date_email')->nullable(true);
            $table->string('type_direction',100)->nullable(true);
            $table->text('img_room')->nullable(true);
            $table->boolean('status_email')->nullable(true);
            $table->string('id_user',100)->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_email');
    }
}
