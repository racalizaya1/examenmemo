<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id_room');

            $table->string('city_room',150)->nullable(true);
            $table->string('address_room',200)->nullable(true);
            $table->string('coordenates',200)->nullable(true);
            $table->text('img_room')->nullable(true);
            $table->text('description')->nullable(true);
            $table->string('id_user',100)->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
