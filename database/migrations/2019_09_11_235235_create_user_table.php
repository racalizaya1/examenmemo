<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            
            $table->string('first_name',100)->nullable(true);
            $table->string('last_name',100)->nullable(true);
            $table->string('username',200)->nullable(true);
            $table->string('password',300)->nullable(true);
            $table->string('phone',100)->nullable(true);
            $table->string('email',300)->nullable(true);
            $table->boolean('active_user')->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
